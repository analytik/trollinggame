package uglycode.trolling.character.skills
{
	import flash.utils.Dictionary;

	public class Reputation extends Skill
	{
		//public static const :String = '';
		public static const RESPECT:String = 'respectful';
		public static const SARCASM:String = 'sarcastic';
		//public static const HUMOUR:String = 'funny';
		public static const ANAL:String = 'anal retentive';
		public static const BOREDOM:String = 'jaded';
		public static const NICE:String = 'nice';
		public static const INTELLIGENCE:String = 'smart';
		public static const SELF_ESTEEM:String = 'content';
		public static const SANITY:String = 'normal';
		
		
		public static var dictionary:Dictionary;
		
		public function Reputation()
		{
			/**
			 * Adjectives I'd like to use:
			 * witty
			 * hilarious
			 * pestersome (clinginess?)
			 * 
			 * TODO add adjectives for relationships / parents / friends / gaming
			 */
			super();
			dictionary = new Dictionary();
			dictionary[RESPECT] = [
				'a total dick',
				'an asshole',
				'pleasant',
				'respectful'
			];
			dictionary[SARCASM] = [
				"someone who doesn't understand humour",
				'unfunny',
				'sarcastic',
				'sarcastic bitch'
			];
			dictionary[ANAL] = [
				"forgiving",
				'nice',
				'anal retentive',
				'insufferable prick'
			];
			dictionary[BOREDOM] = [
				"interesting",
				'active',
				'bored',
				'jaded'
			];
			dictionary[INTELLIGENCE] = [
				"obviously retarded",
				'dumb',
				'smart',
				'shrewd'
			];
			dictionary[SELF_ESTEEM] = [
				"teeming with insecurities",
				'insecure',
				'content',
				'perfectly happy being yourself'
			];
			dictionary[SANITY] = [
				"visibly insane",
				'disturbed',
				'',
				''
			];
		}
		
		public function translate(name:String, level:uint = 2):String {
			if (dictionary.hasOwnProperty(name) && dictionary[name].hasOwnProperty(level)) {
				trace ('yes, it exists and it is ' + dictionary[name][level]);
				return dictionary[name][level];
			}
			return null;
		}
	}
}