package uglycode.trolling.world.activities
{
	import starling.display.Image;

	public class TumbleDrier extends Activity implements IActivity
	{
		public function TumbleDrier()
		{
			super();
		}
		
		public function get description():String {
			return "It is exactly like watching a tumble drier - millions of images keep swirling in front of you, hypnotizing you. " +
				"You want to make a clothing-inspired art just by looking at it. But you're not. You're just letting it swirl.";
		}
		
		public function get logo():Image {
			return null;
		}
	}
}