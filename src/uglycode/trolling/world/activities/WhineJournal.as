package uglycode.trolling.world.activities
{
	import starling.display.Image;
	
	public class WhineJournal extends Activity implements IActivity
	{
		public function WhineJournal()
		{
			super();
		}
		
		public function get description():String
		{
			return "It's full of whiny kids, which is perfectly fine, because you're one, too.";
		}
		
		public function get logo():Image
		{
			return null;
		}
	}
}