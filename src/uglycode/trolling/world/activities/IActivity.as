package uglycode.trolling.world.activities
{
	import starling.display.Image;

	public interface IActivity
	{
		function get description():String;
		function get logo():Image;
	}
}