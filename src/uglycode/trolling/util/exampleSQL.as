import fl.controls.DataGrid;
import fl.controls.dataGridClasses.DataGridColumn;

//set up the basic app functionality
quit_btn.addEventListener(MouseEvent.CLICK,quitApp);
minimize_btn.addEventListener(MouseEvent.CLICK,minimizeApp);
bg.drag_mc.addEventListener(MouseEvent.MOUSE_DOWN,dragApp);

function quitApp(e:MouseEvent):void {
	NativeApplication.nativeApplication.exit();
}
function minimizeApp(e:MouseEvent):void {
	this.stage.nativeWindow.minimize();
}
function dragApp(e:MouseEvent):void {
	this.stage.nativeWindow.startMove();
}

//format the datagrid
function formatGrid(){
	var idCol:DataGridColumn = new DataGridColumn("ID");
	idCol.headerText = "ID";
	idCol.width = 40;
	
	var nameCol:DataGridColumn = new DataGridColumn("Name");
	nameCol.headerText = "Name";
	nameCol.width = 300;
	
	var telephoneCol:DataGridColumn = new DataGridColumn("Telephone");
	telephoneCol.headerText = "Telephone";
	telephoneCol.width = 180;

	contacts_dg.columns = [idCol, nameCol, telephoneCol];
	
	var gridTextFormat:TextFormat = new TextFormat();
	gridTextFormat.color = "0xFFFFFF";
	contacts_dg.setStyle("headerTextFormat", gridTextFormat);
	contacts_dg.setRendererStyle("textFormat", gridTextFormat);
}

formatGrid();


//The database part
//create a new sql connection
var conn:SQLConnection= new SQLConnection();
//add an event handler for the open event
conn.addEventListener(SQLEvent.OPEN, openHandler);
//create the database if it doesn't exist, otherwise just opens it
var dbFile:File=File.applicationDirectory.resolvePath("simple_contact.db");
conn.openAsync(dbFile);

function openHandler(event:SQLEvent):void {
	//create a new sql statement
	var sql:SQLStatement=new SQLStatement();
	//set the statement to connect to our database
	sql.sqlConnection=conn;
	//parse the sql command that creates the table if it doesn't exist
	sql.text= "CREATE TABLE IF NOT EXISTS contact(id INTEGER PRIMARY KEY AUTOINCREMENT, cName TEXT, cTelephone INTEGER)";
	//add a new event listener to the sql when it completes creating the table
	sql.addEventListener(SQLEvent.RESULT,retrieveData);
	//call the execute function to execute our statement
	sql.execute();
}

function retrieveData(event:SQLEvent = null):void {
	//create a new sql statemant
	var sql:SQLStatement = new SQLStatement();
	sql.sqlConnection=conn;
	//this sql command retrieves all the fields from our table in the database and orders it by id
	sql.text =  "SELECT id, cName, cTelephone FROM contact ORDER BY id";
	//add a new event listener if there is data to display it
	sql.addEventListener(SQLEvent.RESULT, populateDataGrid);
	sql.execute();
}

function populateDataGrid(event:SQLEvent):void {
	//first we clear the datagrid
	contacts_dg.removeAll();
	//then create a result variable that holds all our contacts
	var result:SQLResult=event.target.getResult();
	//we check if results is not empty
	if (result!=null&&result.data!=null) {
		//and we add a new item to our datagrid for each contact in our database
		for (var i:Number = 0; i < result.data.length; i++) {
			contacts_dg.addItem ({ ID:result.data[i].id, Name:result.data[i].cName, Telephone:result.data[i].cTelephone});
		}
	}
}

contacts_dg.addEventListener(Event.CHANGE, changeHandler);
function changeHandler(e:Event):void{
	cID.text = contacts_dg.selectedItem.ID;
	cName.text = contacts_dg.selectedItem.Name;
	cTelephone.text = contacts_dg.selectedItem.Telephone;
}

create_btn.addEventListener(MouseEvent.CLICK, createToDatabase);
read_btn.addEventListener(MouseEvent.CLICK, readFromDatabase);
update_btn.addEventListener(MouseEvent.CLICK, updateToDatabase);
delete_btn.addEventListener(MouseEvent.CLICK, deleteFromDatabase);
reset_btn.addEventListener(MouseEvent.CLICK, reset);

//CREATE
function createToDatabase(e:MouseEvent){
	if(cName.text != "" && cTelephone.text != ""){
		var sql:SQLStatement=new SQLStatement();
		sql.sqlConnection=conn;
		//this sql command creates a new entry in the database using the data from the input fields
		sql.text =  "INSERT INTO contact(cName, cTelephone)VALUES(@cName, @cTelephone)";
		sql.parameters["@cName"]=cName.text;
		sql.parameters["@cTelephone"]=cTelephone.text;
		sql.addEventListener(SQLEvent.RESULT,retrieveData);
		sql.execute();
		clearText();
	}
}
//READ
function readFromDatabase(e:MouseEvent){
	var sql:SQLStatement = new SQLStatement();
	sql.sqlConnection=conn;
	//this sql command retrieves all the fields from our table in the database where the name matches the name input
	sql.text =  "SELECT id, cName, cTelephone FROM contact WHERE cName LIKE '%" + cName.text + "%'";
	sql.addEventListener(SQLEvent.RESULT, populateDataGrid);
	sql.execute();
}
//UPDATE
function updateToDatabase(e:MouseEvent){
	if(cName.text != "" && cTelephone.text != ""){
		var sql:SQLStatement=new SQLStatement();
		sql.sqlConnection=conn;
		//this sql command updates the selected entry in the datagrid based on the data in the input fields
		sql.text = "UPDATE contact SET cName='" + cName.text + "', cTelephone='" + cTelephone.text + "' WHERE ID=" + cID.text;
		sql.addEventListener(SQLEvent.RESULT,retrieveData);
		sql.execute();
		clearText();
	}
}
//DELETE
function deleteFromDatabase(e:MouseEvent){
	if(cName.text != "" && cTelephone.text != ""){
		var sql:SQLStatement=new SQLStatement();
		sql.sqlConnection=conn;
		//this sql command deletes the entry from the database with the same id as the selected one in the datagrid
		sql.text = "DELETE FROM contact WHERE ID=" + cID.text;
		sql.addEventListener(SQLEvent.RESULT,retrieveData);
		sql.execute();
		clearText();
	}
}

function reset(e:MouseEvent){
	retrieveData(null);
	clearText();
}

function clearText(){
	cID.text = "";
	cName.text = "";
	cTelephone.text = "";
}

cName.restrict="a-zA-Z ";
cTelephone.restrict="0-9";
