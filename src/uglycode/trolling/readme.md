# Source code of TrollingGame #

## character ##
Containts things related to the player.

- disorders: disorders and diseases and symptoms, both short- and long-term, both mental and physical
- skills: trades that can be learned (and unlearned) like drawing. Affects ability to communicate and create things and be funny, etc.
- skills/Reputation: you can have a reputation for any Skill or Topic.
- traits: (predisposition) laziness, self-esteem, need for approval, clingyness, superiority complex
- variables: (attributes) daily variables like BodyActivityIndex, Creativity, Energy, PeerPressure, SexualFrustration, SleepDeprivation, Somnolence, Willpower, etc
- 