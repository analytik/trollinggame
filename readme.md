# TrollingGame #

## About ##

This is a simple time management / strategy game about internet usage. You cannot win this game. Every action has a consequence, and most actions have hidden long-term consequences on your health and mental well-being.

It isn't meant to be an exact simulation, but I've tried to base the game rules on real effects of Internet usage, although mostly it is a parody of life.

## Technology ##

AS3/Flash/AIR

## Features for 1GaMo release ##
- several sites and actions
- health
- time
- school
- blog
- random events

## Features for 1.0 release ##
- several sites you can waste time on
- Ability to have a blog / hobby / software project
- school / job / family / life / spouse
- real-world relationship issues
- disorders
- money management, buying gadgets
- food management